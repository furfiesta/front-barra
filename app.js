new Vue({
  el: '#app',
  data: {
    showStep: [true, false, false],
    barcode: null,
    showError: {
      notFound: false,
      lastBarcode: null,
      broken: false,
    },
    user: null,
    isLoading: false,
    isLoadingRemaining: false,
    remainingDrinkTickets: 0,
    isSubmitting: false,
    cannotOperate: false,
    response: {
      replied: false,
      ok: false,
    }
  },
  created: function () {
    setInterval(() => {
      this.$refs.barcodeInput.focus()
    }, 300)
  },
  methods: {
    resetAll() {
      this.user = null
      this.isLoading = false
      this.isLoadingRemaining = false
      this.remainingDrinkTickets = 0
      this.barcode = null
      this.cannotOperate = false,
      this.showError = {
        notFound: false,
        lastBarcode: null,
        broken: false,
      }
      this.response = {
        replied: false,
        ok: false,
      }
      this.changeTo(1)
    },
    searchUser: function () {
      if (this.barcode) {
        this.isLoading = true
        axios
          .get(`http://192.168.0.45:9998/conops/userFromBarcode?token=no_me_estan_pagando_lo_suficiente_por_esto&barcode=${this.barcode}`)
          .then(response => {
            this.user = response.data
            this.changeTo(2)
            this.barcode = null
            this.isLoading = false
            this.getRemainingDrinks()
            return
          }).catch(err => {
            if(err.response.data.message == 'user not found'){
              this.showError.notFound = true;
            } else {
              this.showError.broken = true
            }
            this.showError.lastBarcode = this.barcode
            this.barcode = null;
            this.isLoading = false
            console.error(err)
          })
      }
    },
    useDrink: function () {
      this.isSubmitting = true
      this.user.attendeeType = null
      this.changeTo(3)
      axios
        .get(`http://192.168.0.45:9998/conops/useDrinkTicket?token=no_me_estan_pagando_lo_suficiente_por_esto&email=${this.user.email}`)
        .then(response => {
          this.isSubmitting = false
          this.response.ok = true
          this.response.replied = true
          return
        }).catch(err => {
          this.isSubmitting = false
          this.response.replied = true
          console.error(err)
        })
    },
    sellDrink: function () {
      this.isSubmitting = true
      this.user.attendeeType = null
      this.changeTo(3)
      axios
        .get(`http://192.168.0.45:9998/conops/sellDrink?token=no_me_estan_pagando_lo_suficiente_por_esto&email=${this.user.email}`)
        .then(response => {
          this.isSubmitting = false
          this.response.ok = true
          this.response.replied = true
          return
        }).catch(err => {
          this.isSubmitting = false
          this.response.replied = true
          console.error(err)
        })
    },
    getRemainingDrinks: function () {
      this.isLoadingRemaining = true
      axios.get(
        `http://192.168.0.45:9998/conops/remainingDrinkTickets?token=no_me_estan_pagando_lo_suficiente_por_esto&email=${this.user.email}`
      ).then(response => {
        this.remainingDrinkTickets = response.data.remainingDrinkTickets
        this.isLoadingRemaining = false
      }).catch(err => {
        this.cannotOperate = true
        this.isLoadingRemaining = false
        console.error(err)
      })
    },
    changeTo: function (step) {
      this.showStep = [false, false, false];
      this.showStep[step - 1] = true;
      if (step == 1) {
        this.$nextTick(() => this.$refs.barcodeInput.focus())
      }
    }
  },
  computed: {
    userAvatar: function () {
      return `http://192.168.0.45:9998/avatar/${this.user.fursonaAvatar}`
    },
    isMinor: function () {
      if (this.user) {
        if (this.user.attendeeType == 'menor' || this.user.attendeeType == 'medio') return true
      }
      return false
    },
    returnHeroColor: function () {
      if (this.isMinor) return 'is-danger'
      if (!this.isMinor) {
        if (this.response.replied && this.response.ok) return 'is-success'
        if (this.response.replied && !this.response.ok) return 'is-danger'
        return 'is-black'
      }
    }
  }
})